var OrderContract = artifacts.require("./OrderContract.sol");
var ItemToken = artifacts.require("./ItemToken.sol");

module.exports = function(deployer) {
  deployer.deploy(OrderContract, "Product One", 1000000000000000000);
  deployer.deploy(ItemToken, 5);
};
