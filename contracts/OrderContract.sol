pragma solidity ^0.4.11;

import "./ItemToken.sol";

contract OrderContract {

    address public seller;
    address public customer;
    address public bank;
    address public manufacturer;
    uint public orderSize = 0;
    uint public productPrice = 0;
    string public productName;
    enum Status { INIT, ORDER_PLACED, CREDITED, MANUFACTURER_REQUESTED, ACCEPTED_BY_MANUFACTURER, WORK_FINISHED, ACCEPTED_BY_SHIPPER, FINISHED }
    Status public status;
    ItemToken tokenContract;

    function OrderContract(string _productName, uint _productPrice) public {
        seller = msg.sender;
        productName = _productName;
        productPrice = _productPrice;
    }

    function placeOrder(uint _orderSize) public isNotSeller isOrderAvailable {
        customer = msg.sender;
        orderSize = _orderSize;
        status = Status.ORDER_PLACED;
    }

    function fundOrder() payable public isFullPayment isPaymentPossible {
        bank = msg.sender;
        status = Status.CREDITED;
    }

    function sendRequestToManufacturer(address _manufacturer) public isPaymentAvailable isSeller {
        manufacturer = _manufacturer;
        status = Status.MANUFACTURER_REQUESTED;
    }

    function acceptManufacturingRequest() public isManufacturer isManufacturingAvailable {
        status = Status.ACCEPTED_BY_MANUFACTURER;
        tokenContract = new ItemToken(orderSize);
        tokenContract.myPayableFunction.value(this.balance)();
        tokenContract.transfer(manufacturer, orderSize);
    }

    function acceptShippingRequest() public isOrderShippable {
        status = Status.ACCEPTED_BY_SHIPPER;
        tokenContract.transferFrom(manufacturer, msg.sender, orderSize);
    }

    function finishWork() public isWorkFinishable {
        status = Status.WORK_FINISHED;
        tokenContract.setDone();
    }

    function confirmShipping() public isCustomer {
        status = Status.FINISHED;
        tokenContract.kill();
    }

    modifier isCustomer() {
        require(msg.sender == customer);
        _;
    }

    modifier isOrderShippable() {
        require(status == Status.WORK_FINISHED && tokenContract.balanceOf(manufacturer) == orderSize);
        _;
    }

    modifier isWorkFinishable() {
        require(!tokenContract.isDone());
        _;
    } 

    modifier isManufacturer() {
        require(msg.sender == manufacturer);
        _;
    }

    modifier isManufacturingAvailable() {
        require(status == Status.MANUFACTURER_REQUESTED);
        _;
    }

    modifier isSeller() {
        require(msg.sender == seller);
        _;
    }

    modifier isNotSeller() {
        require(msg.sender != seller);
        _;
    }

    modifier isFullPayment() {
        require(msg.value == (orderSize * productPrice));
        _;
    }

    modifier isOrderAvailable() {
        require(orderSize == 0);
        _;
    }

    modifier isPaymentAvailable() {
        require(bank != address(0));
        _;
    }

    modifier isPaymentPossible() {
        require(bank == address(0) && orderSize != 0);
        _;
    }
}