pragma solidity ^0.4.11;

import "./ERC20TokenInterface.sol";

contract ItemToken is ERC20Interface {

    address public orderContract;
    address internal owner;
    string public symbol;
    string public name;
    uint8 public decimals;
    uint public _totalSupply;
    bool public isDone = false;

    mapping(address => uint) balances;
    mapping(address => mapping(address=>uint)) allowed;


    function ItemToken(uint _orderSize) public {
        symbol = "ITEMTOKEN";
        name = "Item Token";
        decimals = 0;
        _totalSupply = _orderSize;
        orderContract = msg.sender;
        owner = tx.origin;
        balances[orderContract] = _totalSupply;
        isDone = true;
    }

    function myPayableFunction() public payable returns (bool result) {
        return true; 
    }

    function totalSupply() public constant returns (uint) {
        return _totalSupply - balances[address(0)];
    }

    function balanceOf(address tokenOwner) public constant returns (uint balance) {
        return balances[tokenOwner];
    }

    function allowance(address tokenOwner, address spender) public constant returns (uint remaining) {
        return allowed[tokenOwner][spender];
    }

    function transfer(address to, uint tokens) public returns (bool success) {
        if (isDone) {
            balances[msg.sender] -= tokens;
            balances[to] += tokens;
            isDone = false;
            owner = tx.origin;
            return true;
        } else {
            revert();
        }
    }

    function approve(address spender, uint tokens) public returns (bool success) {
        allowed[msg.sender][spender] = tokens;
        Approval(msg.sender, spender, tokens);
        return true;
    }

    function transferFrom(address from, address to, uint tokens) public returns (bool success) {
    if (msg.sender != orderContract && !isDone) { revert();}
        balances[from] -= tokens;
        balances[to] += tokens;
        owner = tx.origin;
        isDone = false;
        return true;
    }

    function setDone() public isOwner isOrderContract {
        isDone = true;
        tx.origin.transfer(this.balance/2);
    }

    function kill() public {
        selfdestruct(owner);
    }
 
    modifier isOwner() {
        require(balanceOf(tx.origin) == _totalSupply);
        _;
    }

    modifier isOrderContract() {
        require(msg.sender == orderContract);
        _;
    }

    event Approval(address indexed tokenOwner, address indexed spender, uint tokens);
}