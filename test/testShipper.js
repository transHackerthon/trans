const OrderContract = artifacts.require("./OrderContract.sol");

function getBalance(address) {
    return new Promise(function (resolve, reject) {
        web3.eth.getBalance(address, function (error, result) {
            if (error) {
                reject(error);
            } else {
                resolve(result.toNumber());
            }
        });
    });
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

contract('Shipping contract', function (accounts) {
    let orderContract;
    const seller = accounts[0];
    const customer = accounts[1];
    const bank = accounts[2];

    const manufacturer = accounts[3];
    const shipper = accounts[4];
    const dummy = accounts[9];
    const productName = "Product One"

    const orderSize = 5;
    const oneEtherValue = 1000000000000000000;

    beforeEach(function () {
        return OrderContract.new(productName, oneEtherValue, { from: seller })
            .then(function (instance) {
                orderContract = instance;
            })
            .then(() => orderContract.placeOrder(orderSize, { from: customer }))
            .then(() => orderContract.fundOrder({ from: bank, value: (orderSize * oneEtherValue) }))
            .then(() => orderContract.sendRequestToManufacturer(manufacturer, { from: seller }))
            .then(() => orderContract.acceptManufacturingRequest({ from: manufacturer }))
            .then(() => orderContract.finishWork({ from: manufacturer }))
    });

    it("should accept shipping request", function () {
        return orderContract.acceptShippingRequest({ from: shipper })
            .then(() => orderContract.status())
            .then(result => {
                assert.equal(result, 6);
            });
    });
    describe("working", function () {
        beforeEach(function () {
            return OrderContract.new(productName, oneEtherValue, { from: seller })
                .then(function (instance) {
                    orderContract = instance;
                })
                .then(() => orderContract.placeOrder(orderSize, { from: customer }))
                .then(() => orderContract.fundOrder({ from: bank, value: (orderSize * oneEtherValue) }))
                .then(() => orderContract.sendRequestToManufacturer(manufacturer, { from: seller }))
                .then(() => orderContract.acceptManufacturingRequest({ from: manufacturer }))
                .then(() => orderContract.finishWork({ from: manufacturer }))
                .then(() => orderContract.acceptShippingRequest({ from: shipper }));
        });

        it("finishing work", function () {

            let shipperBalance;
            return getBalance(shipper)
                .then(result => {
                    shipperBalance = result;
                })
                .then(() => orderContract.finishWork({ from: shipper }))
                .then(() => orderContract.status())
                .then(result => {
                    assert.equal(result, 5);
                })
                .then(() => sleep(2000))
                .then(() => getBalance(shipper))
                .then(result => {
                    assert.ok(+shipperBalance < +result);
                });
        });
        it("not accepting finish work twice", function () {
            return orderContract.finishWork({ from: shipper })
                .then(() => orderContract.status())
                .then(result => {
                    assert.equal(result, 5);
                }).then(() => orderContract.finishWork({ from: shipper }))
                .then(result => {
                    assert.fail("no work has started");
                }, err => {
                    assert.equal(err.message, "VM Exception while processing transaction: revert")
                });
        });

        it("not able to finish work on the wrong account", function () {
            return orderContract.finishWork({ from: dummy })
                .then(() => orderContract.status())
                .then(result => {
                    assert.fail("no work has started");
                }, err => {
                    assert.equal(err.message, "VM Exception while processing transaction: revert")
                });
        });
    })
})