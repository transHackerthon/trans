const OrderContract = artifacts.require("./OrderContract.sol");

function getBalance(address) {
    return new Promise(function (resolve, reject) {
        web3.eth.getBalance(address, function (error, result) {
            if (error) {
                reject(error);
            } else {
                resolve(result.toNumber());
            }
        });
    });
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

contract('consumer', function (accounts) {
    let orderContract;
    const seller = accounts[0];
    const customer = accounts[1];
    const bank = accounts[2];

    const manufacturer = accounts[3];
    const shipper = accounts[4];
    const dummy = accounts[9];
    const productName = "Product One"

    const orderSize = 5;
    const oneEtherValue = 1000000000000000000;

    beforeEach(function () {
        return OrderContract.new(productName, oneEtherValue, { from: seller })
            .then(function (instance) {
                orderContract = instance;
            })
            .then(() => orderContract.placeOrder(orderSize, { from: customer }))
            .then(() => orderContract.fundOrder({ from: bank, value: (orderSize * oneEtherValue) }))
            .then(() => orderContract.sendRequestToManufacturer(manufacturer, { from: seller }))
            .then(() => orderContract.acceptManufacturingRequest({ from: manufacturer }))
            .then(() => orderContract.finishWork({ from: manufacturer }))
            .then(() => orderContract.acceptShippingRequest({ from: shipper }))
            .then(() => orderContract.finishWork({ from: shipper }));
    });

    it("should have correct state", function () {
        return orderContract.confirmShipping({ from: customer })
            .then(() => orderContract.status())
            .then(result => {
                assert.equal(result, 7);
            });
    });

    it("should not be able to confirm shipping on non-started job", function () {
        return orderContract.confirmShipping({ from: dummy })
            .then(() => orderContract.status())
            .then(result => {
                assert.fail("no work has started");
            }, err => {
                assert.equal(err.message, "VM Exception while processing transaction: revert")
            });
    });

    it("should destruct tokens after customer confirms the whole shipping", function () {
        let shipperBalance;
        return getBalance(shipper)
            .then(result => {
                shipperBalance = result;
            })
            .then(() => orderContract.confirmShipping({ from: customer }))
            .then(() => getBalance(shipper))
            .then(result => {
                assert.ok(+shipperBalance < +result);
            });
    });
});