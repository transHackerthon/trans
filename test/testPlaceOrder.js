const OrderContract = artifacts.require("./OrderContract.sol");

contract('Place order', function (accounts) {

    let orderContract;
    const seller = accounts[0];
    const customer = accounts[1];
    const bank = accounts[2];
    const manufacturer = accounts[3];
    const dummy = accounts[9];
    const productName = "Product One"

    const orderSize = 5;
    const oneEtherValue = 1000000000000000000;

    beforeEach(function () {
        return OrderContract.new(productName, oneEtherValue, { from: seller })
            .then(function (instance) {
                assert.ok(instance);
                orderContract = instance;
            })
            .then(() => orderContract.placeOrder(orderSize, { from: customer }))
            ;
    });

    it("should not be able to send a request before the money is received", function () {
        return orderContract.sendRequestToManufacturer(manufacturer, { from: customer })
            .then(() => orderContract.manufacturer())
            .then(result => {
                assert.fail("should not be able to send a request without funds");
            }, err => {
                assert.equal(err.message, "VM Exception while processing transaction: revert")
            });
    });

    it("the bank should not be able to fund the smart contract with a wrong amount", function () {
        return orderContract.fundOrder({ from: bank, value: orderSize })
            .then(() => {
                assert.fail("should not be able to get here with wrong amount");
            }, () => orderContract.bank())
            .then(result => {
                assert.equal(result, "0x0000000000000000000000000000000000000000");
            });
    });
    it("the bank should be able to fund the smart contract", function () {
        return orderContract.fundOrder({ from: bank, value: (orderSize * oneEtherValue) })
            .then(() => orderContract.bank())
            .then(result => {
                assert.equal(result, bank);
            });
    });
});
