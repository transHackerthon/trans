const OrderContract = artifacts.require("./OrderContract.sol");

function getBalance(address) {
    return new Promise(function (resolve, reject) {
        web3.eth.getBalance(address, function (error, result) {
            if (error) {
                reject(error);
            } else {
                resolve(result.toNumber());
            }
        });
    });
}

contract('manufacturing applying', function (accounts) {
    let orderContract;
    const seller = accounts[0];
    const customer = accounts[1];
    const bank = accounts[2];
    const manufacturer = accounts[3];
    const dummy = accounts[9];
    const productName = "Product One"

    const orderSize = 5;
    const oneEtherValue = 1000000000000000000;

    beforeEach(function () {
        return OrderContract.new(productName, oneEtherValue, { from: seller })
            .then(function (instance) {
                orderContract = instance;
            })
            .then(() => orderContract.placeOrder(orderSize, { from: customer }))
            .then(() => orderContract.fundOrder({ from: bank, value: (orderSize * oneEtherValue) }))
            .then(() => orderContract.sendRequestToManufacturer(manufacturer, { from: seller }));
    });

    it("should be manufacturer to apply", function () {
        return orderContract.acceptManufacturingRequest({ from: dummy })
            .then(() => orderContract.status())
            .then(result => {
                assert.fail("should not be able to apply as dummy");
            }, err => {
                assert.equal(err.message, "VM Exception while processing transaction: revert")
            });
    });


    it("accept request", function () {
        return orderContract.acceptManufacturingRequest({ from: manufacturer })
            .then(() => orderContract.status())
            .then(result => {
                assert.equal(result, 4);
            });
    });
    describe("sent request to manufacturer", function () {
        beforeEach(function () {
            return OrderContract.new(productName, oneEtherValue, { from: seller })
                .then(function (instance) {
                    orderContract = instance;
                })
                .then(() => orderContract.placeOrder(orderSize, { from: customer }))
                .then(() => orderContract.fundOrder({ from: bank, value: (orderSize * oneEtherValue) }))
                ;
        });

        it("the customer should not be able to sent a request to the manufacturer", function () {
            return orderContract.sendRequestToManufacturer(manufacturer, { from: customer })
                .then(() => orderContract.manufacturer())
                .then(result => {
                    assert.fail("should not be able to send a request from the customer");
                }, err => {
                    assert.equal(err.message, "VM Exception while processing transaction: revert")
                });
        });

        it("should not be able to apply as manufacturer before requests are sent", function () {
            return orderContract.acceptManufacturingRequest({ from: customer })
                .then(() => orderContract.status())
                .then(result => {
                    assert.fail("should not be able to apply yet");
                }, err => {
                    assert.equal(err.message, "VM Exception while processing transaction: revert")
                });
        });

        it("the seller should be able to sent a request to the manufacturer", function () {
            return orderContract.sendRequestToManufacturer(manufacturer, { from: seller })
                .then(() => orderContract.manufacturer())
                .then(result => {
                    assert.equal(result, manufacturer);
                });
        });
    })

    describe("manufacturer finishes work", function () {
        beforeEach(function () {
            return OrderContract.new(productName, oneEtherValue, { from: seller })
                .then(function (instance) {
                    orderContract = instance;
                })
                .then(() => orderContract.placeOrder(orderSize, { from: customer }))
                .then(() => orderContract.fundOrder({ from: bank, value: (orderSize * oneEtherValue) }))
                .then(() => orderContract.sendRequestToManufacturer(manufacturer, { from: seller }))
                .then(() => orderContract.acceptManufacturingRequest({ from: manufacturer }));
        });


        it("finish production by wrong user", function () {
            return orderContract.finishWork({ from: dummy })
                .then(result => {
                    assert.fail("no work has started");
                }, err => {
                    assert.equal(err.message, "VM Exception while processing transaction: revert")
                });
        });

        it("should not be able to accept shipping requests if manufacturer is not finished", function () {
            return orderContract.acceptShippingRequest({ from: dummy })
                .then(result => {
                    assert.fail("no work has started");
                }, err => {
                    assert.equal(err.message, "VM Exception while processing transaction: revert")
                });
        });

        it("finish production", function () {
            let manufacturerBalance;
            getBalance(manufacturer)
                .then(result => {
                    manufacturerBalance = result;
                })
                .then(() => orderContract.finishWork({ from: manufacturer }))
                .then(() => orderContract.status())
                .then(() => getBalance(manufacturer))
                .then(result => {
                    assert.ok(+manufacturerBalance < +result);
                });
        });
    });
});