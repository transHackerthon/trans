const ItemToken = artifacts.require("./ItemToken.sol");

function getBalance(address) {
    return new Promise(function (resolve, reject) {
        web3.eth.getBalance(address, function (error, result) {
            if (error) {
                reject(error);
            } else {
                resolve(result);
            }
        });
    });
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

contract('ItemToken', function (accounts) {

    let itemToken;
    const orderSize = 5;
    const name = "Item Token";
    const symbol = "ITEMTOKEN";
    const seller = accounts[0];
    const manifactory = accounts[1];
    const bank = accounts[2];
    const oneEther = 1000000000000000000;

    beforeEach(function () {
        return ItemToken.new(orderSize, { from: seller })
            .then(function (instance) {
                assert.ok(instance);
                itemToken = instance;
                itemToken.myPayableFunction({from: seller, value: oneEther});
            });
    });
    it("token should have balance of one ether", function () {
        return getBalance(itemToken.address)
            .then(result => {
                assert.equal(oneEther, result);
            })
    });
    it("seller should be the token owner after created it", function () {
        return itemToken.orderContract()
            .then(owner => {
                assert.equal(seller, owner);
            })
    });
    it("status should be done", function () {
        return itemToken.isDone()
            .then(status => {
                assert.ok(status);
            })
    });
    it("should have a the correct symbol", function () {
        return itemToken.symbol()
            .then(_symbol => {
                assert.equal(symbol, _symbol);
            })
    });
    it("should have a the correct name", function () {
        return itemToken.name()
            .then(_name => {
                assert.equal(name, _name);
            })
    });
    it("should have a the correct totalSupply", function () {
        return itemToken.totalSupply()
            .then(result => {
                assert.equal(orderSize, result);
            })
    });
    it("should have a the correct balance for creator", function () {
        return itemToken.balanceOf(seller, { from: seller })
            .then(result => {
                assert.equal(orderSize, result.toNumber());
            })
    });
    it("should have a the correct balance for bank", function () {
        return itemToken.balanceOf(bank, { from: bank })
            .then(result => {
                assert.equal(0, result.toNumber());
            })
    });
    it("should approve the correct balance for bank", function () {
        return itemToken.approve(bank, orderSize, { from: seller })
            .then(() => itemToken.allowance(seller, bank))
            .then(result => {
                assert.equal(orderSize, result.toNumber());
            })
    });
    it("should be able to transfer tokens from seller to bank", function () {
        return itemToken.transfer(bank, orderSize, { from: seller })
            .then(() => itemToken.balanceOf(seller, { from: seller }))
            .then(result => {
                assert.equal(result.toNumber(), 0);
            })
            .then(() => itemToken.balanceOf(bank, { from: seller }))
            .then(result => {
                assert.equal(orderSize, result.toNumber());
            })
            .then(() => itemToken.isDone({ from: seller }))
            .then(result => {
                assert.ok(!result);
            })
    });

    it("should be able to transfer tokens by ordercontract with transferFrom", function () {
        return itemToken.transferFrom(seller, bank, orderSize, { from: seller })
            .then(() => itemToken.balanceOf(seller, { from: seller }))
            .then(result => {
                assert.equal(result.toNumber(), 0);
            })
            .then(() => itemToken.balanceOf(bank, { from: seller }))
            .then(result => {
                assert.equal(orderSize, result.toNumber());
            })
            .then(() => itemToken.isDone({ from: seller }))
            .then(result => {
                assert.ok(!result);
            })
    });
    it("should not be able to set done as not token owner", function () {
        return itemToken.setDone({ from: manifactory })
            .then(() => {
                assert.fail("should not be here is not token owner tries to finish step");
            }, ()=> { assert.ok(true)});
    });
});