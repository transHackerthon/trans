const OrderContract = artifacts.require("./OrderContract.sol");

contract('OrderContract', function (accounts) {

    let orderContract;
    const seller = accounts[0];
    const customer = accounts[1];
    const bank = accounts[2];
    const manufacturer = accounts[3];
    const dummy = accounts[9];
    const productName = "Product One"

    const orderSize = 5;
    const oneEtherValue = 1000000000000000000;

    beforeEach(function () {
        return OrderContract.new(productName, oneEtherValue, { from: seller })
            .then(function (instance) {
                assert.ok(instance);
                orderContract = instance;
            });
    });
    it("should have a seller", function () {
        orderContract.seller()
            .then(_seller => {
                assert.equal(seller, _seller);
            })
    });
    it("should have productname", function () {
        orderContract.productName()
            .then(_productName => {
                assert.equal(productName, _productName);
            })
    });
    it("seller should not be able to set an order", function () {
        return orderContract.placeOrder(orderSize, { from: seller })
            .then(() => {
                assert.fail("seller should not be able to place an order!");
            }, () => orderContract.orderSize())
            .then(result => {
                assert.equal(result.toNumber(), 0);
            })
    });
    it("customer should be able to place an order", function () {
        return orderContract.placeOrder(orderSize, { from: customer })
            .then(() => orderContract.orderSize())
            .then(result => {
                assert.equal(result.toNumber(), orderSize);
            });
    });
    it("should not be able to place an order if it's already placed", function () {
        return orderContract.placeOrder(orderSize, { from: customer })
            .then(() => orderContract.orderSize())
            .then(result => {
                assert.equal(result.toNumber(), orderSize);
            })
            .then(() => orderContract.placeOrder(orderSize, { from: dummy }))
            .then(result => {
                assert.fail("should not be able to set an order twice");
            }, err => {
                assert.equal(err.message, "VM Exception while processing transaction: revert")
            })
    });
    it("item price should be 1 ether", function () {
        return orderContract.productPrice({ from: customer })
            .then(result => {
                assert.equal(result.toNumber(), oneEtherValue);
            });
    });
});